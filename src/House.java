

import java.util.ArrayList;
import java.util.Scanner;

public class House {
    ArrayList<Animal> animalList = new ArrayList<Animal>();

    public House() {

    }

    public void putAnimalInHouse() {

        System.out.print("Nice house. Want to put some animals? y|n\n>");

        Animal animal = null;
        Scanner sc = new Scanner(System.in);
        String choise;

        choise = sc.next();

        while (choise.charAt(0) == 'y') {
            System.out.print("What animal? dog|cat|parrot\n\t>");
            choise = sc.next();


            if (choise.equals("cat")) {

                if (isDogThere()) {
                    System.out.println("cats don't like dogs");
                } else {
                    animal = new Animal("cat");
                    animalList.add(animal);
                    System.out.println(animal.makeSound());
                    if (isPerrotThere()) {
                        removeParrot();
                        System.out.println("parrot eaten");
                    }
                }
            } else if (choise.equals("dog")) {
                if(isCatThere())
                {
                    System.out.println("dogs don't like cats");
                } else {
                    animal = new Animal("dog");
                    animalList.add(animal);
                    System.out.println(animal.makeSound());
                }
            } else if (choise.equals("parrot")) {
                if (isCatThere()) {
                    System.out.println("that cat ate me");
                } else {
                    animal = new Animal("parrot");
                    animalList.add(animal);
                    System.out.println(animal.makeSound());
                }
            }


            System.out.print("More animals? y|n\n\t>");
            choise = sc.next();

        }

        animalList.forEach((n) -> System.out.println("You have a " + n.getAnimalType() + " who does " + n.makeSound() + " sound"));


    }


    private boolean isPerrotThere() {
        boolean isparrotthere = false;

        for (var pet : animalList
        ) {
            if (pet.getAnimalType().equals("parrot")) {

                isparrotthere = true;
            }
        }

        return isparrotthere;
    }

    private boolean isCatThere() {
        boolean iscatThere = false;

        for (var pet : animalList
        ) {
            if (pet.getAnimalType().equals("cat")) {

                iscatThere = true;
            }
        }

        return iscatThere;
    }

    private boolean isDogThere() {
        boolean dogThere = false;

        for (var pet : animalList
        ) {
            if (pet.getAnimalType().equals("dog")) {

                dogThere = true;
            }
        }

        return dogThere;
    }

    private void removeParrot() {

        for (int i = 0; i < animalList.size(); ) {
            if (animalList.get(i).getAnimalType().equals("parrot")) {
                animalList.remove(i);
            } else {
                i++;
            }
        }


    }


    public void knockKnock(){
        for (var pet:animalList
             ) {
            System.out.println(pet.makeSound());
        }
    }

}
